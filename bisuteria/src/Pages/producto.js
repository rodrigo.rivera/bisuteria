import React from 'react';
import { useParams } from "react-router-dom";

import { Descripcion } from "../Componentes/Descripcion";

export function ProductoDetalle(params) {

    
    
    function DescripcionProducto() {
        let { id } = useParams();
         console.log("[DescripcionProducto] url id: ", id) 
        return (<Descripcion producto={id} />)
    }

    return (
        <>
            
            <DescripcionProducto />
            
        </>
    )
}