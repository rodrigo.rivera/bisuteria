import React from 'react';
import { Carrusel } from "../Componentes/Carrusel";
import { Populares } from "../Componentes/Pupulares";
import { Catalogo } from "../Componentes/Catalogo";
import  "../Styles/home.css";

import { Divider, Layout } from 'antd';

const { Content } = Layout;

export function Home(props) {

    console.log(props)
    


    return (
        <>
            {/* <GetMenu foo={props.menu} /> */}
            <Carrusel />
            <Content >
                <div className="home" >
                    <br/>
                    <Divider orientation="left">Catalogo</Divider>
                    <Catalogo item={props.indexMenu.indexMenu} />
                    <Divider orientation="left">Productos Mas vendidos</Divider>
                    <Populares />
                </div>
            </Content>
            {/* <MessengerCustomerChat
                pageId="105389628270243"
                appId="575137709847776"
            /> */}

        </>
    )
}