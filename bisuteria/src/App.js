import React, { useEffect, useState } from 'react';

import 'antd/dist/antd.css';
import './index.css';
import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";
import { ProductoDetalle } from "./Pages/producto";
import { Home } from "./Pages/home";
import { MiHeader } from "./Componentes/MiHeader";
import { MiFooter } from "./Componentes/MiFooter";
import { Layout } from 'antd';
import DynamicAntdTheme from 'dynamic-antd-theme';
import MessengerCustomerChat from 'react-messenger-customer-chat';





function App() {

  const [indexMenu, setIndexMenu] = useState("Aretes");


  const menuClick = e => {
    console.log('click ', e.key);
    setIndexMenu(e.key)
  };

  useEffect(() => {
    console.log("Recargar")
  });

  const customCss = `
  body {
    font-family: 'Lobster', cursive;
    

  }
`;



  return (
    <>
      <Router>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet" />

        <Layout style={{ minHeight: "100vh" }}>
          <MiHeader menu={menuClick} />
          <Switch>
            <Route path={`/producto/:id`}>
              <ProductoDetalle menu={menuClick} />
            </Route>
            <Route path="/home">
              <Home menu={menuClick} indexMenu={indexMenu} />
            </Route>
            <Route path="/">
              <Redirect to="/home" />
            </Route>

          </Switch>
          <DynamicAntdTheme customCss={customCss} />
          <MessengerCustomerChat
            pageId="1895382890692545"
            appId="215971755540323"
            
          />

          <MiFooter />
        </Layout>
      </Router>

    </>
  );

}



export default App;
