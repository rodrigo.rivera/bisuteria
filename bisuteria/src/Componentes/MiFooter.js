import React from "react";
import { Avatar, Image } from 'antd';
import IconoFacebook from "../Assets/Redes/facebook.svg";
import IconoInstagram from "../Assets/Redes/instagram.svg";
import IconoWhatApp from "../Assets/Redes/whatsapp.svg";
import  "../Styles/footer.css";

export function MiFooter(){

    return (
        <div class="footer-basic">
        
            <div class="social">
            <a href="www.google.com.mx"><Avatar shape="square"
                src={<Image src={IconoFacebook} preview={false} />}
            /></a>
            <a href="www.google.com.mx"><Avatar shape="square"
                src={<Image src={IconoInstagram} preview={false} />}
            /></a>
            <a href="https://api.whatsapp.com/send/?phone=525547711176&text=Me+interesa+estos+produrctos+&app_absent=0"><Avatar shape="square"
                src={<Image src={IconoWhatApp} preview={false} />}
            /></a>
            
           </div>

            <p class="copyright">Company Name © 2018</p>
        
    </div>
    );
}