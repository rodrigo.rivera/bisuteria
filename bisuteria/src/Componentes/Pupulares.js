import React from "react";
import { Cart } from "../Componentes/Cart";
import { GetPopulares } from "../Services/REST";
import { Row, Col} from 'antd';

export function Populares(props) {

    let data = GetPopulares();
    return (
        <Row gutter={[16, 24]}>
            {
                data.map((it, index) => {
                    return (
                        <Col xs={{ span: 24 }} md={{ span: 8 }} lg={{ span:8 }} >
                            <Cart key={index} data={it} click={props.seleccionar} />
                        </Col>
                    )
                })
            }
        </Row>
    );
}