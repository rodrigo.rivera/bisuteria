import React from "react"
import { Menu, Avatar, Image } from 'antd';
import IconoArete from "../Assets/menu/arete.svg";
import "../Styles/menu.css";
import IconoCadena from "../Assets/menu/collar.svg";
import IconoTobilleras from "../Assets/menu/collar2.svg";
import IconoConjunto from "../Assets/menu/collar3.svg";
import { Link } from "react-router-dom";

import { Layout } from 'antd';

const { Header,  } = Layout;


export function MiHeader(params) {
    let stilos = {
        "margin": " 0",
        "width": "100%",
        "left":0,
        "list-style-type": " none",
        "background": "#fda5b4",        
        "position": "fixed",
        display: 'flex',

        justifyContent: 'center',

        ".ant-menu-submenu": { "margin-top": "15px" }

    }
 
    //console.log("GetMenu",params);

    return (
        <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }} >
            <Menu mode="horizontal" style={stilos} onClick={params.menu}>

                <Menu.Item key="Aretes" icon={<Avatar shape="square"
                    src={<Image src={IconoArete} preview={false} />}
                />}>
                    {/* <a href="/home/aretes">Aretes</a> */}
                    <Link to="/home" replace >Aretes</Link>

                </Menu.Item>
                <Menu.Item key="Collares" icon={<Avatar shape="square"
                    src={<Image src={IconoCadena} preview={false} />}
                />}>
                    <Link to="/home" replace >Collares</Link>

                </Menu.Item>

                <Menu.Item key="Tobilleras" icon={<Avatar shape="square"
                    src={<Image src={IconoTobilleras} preview={false} />}
                />}>
                    <Link to="/home" replace >Tobilleras</Link>

                </Menu.Item>

                <Menu.Item key="Conjuntos" icon={<Avatar shape="square"
                    src={<Image src={IconoConjunto} preview={false} />}
                />}>
                    <Link to="/home" replace >Conjuntos</Link>

                </Menu.Item>
            </Menu>

        </Header>
    )

}

