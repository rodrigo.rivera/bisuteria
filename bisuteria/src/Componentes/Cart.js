import React from "react";
import { Card, Image } from 'antd';
import "../Styles/cart.css";
import { motion } from "framer-motion";
import { Link } from "react-router-dom";



export function Cart(data) {

    //console.log("[cart]Dat ", data)
    let imgErro = "img/error.png";

    let classAgotado = data.data.disponible ? "" : "agotado";

    return (
        < > 
            { data !== undefined || data !== null ?
                <Card id={data.data.id} cover={
                    <>
                    <motion.div
                    whileHover={{ scale: 1.2 }}
                    whileTap={{ scale: 1.9 }}
                  />
                  <Link to={"/producto/"+data.data.id}>
                    <Image  preview={false}
                        height={"30vh"}
                        width={"100%"}                        
                        id={data.data.id}                    
                        src={data.data.url}
                    ></Image>
                    </Link>
                    </>
                }>
                    {/* <Meta title={data.data.titulo} description={data.data.descripcion} /> */}
                    <div className={"centrar"}>
                    <Link to={"/producto/"+data.data.id}>id:{data.data.id}</Link>                    
                        <h1 className={classAgotado} >${data.data.precio}</h1>
                        <p className={classAgotado} >{data.data.titulo} </p>
                        <p className={classAgotado} >{data.data.disponible ? "Disponible" : "Agotado"}</p>
                    </div>
                </Card>
                :
                <Card
                    cover={<img src={imgErro} alt="Error imagen" />}
                >
                </Card>
            }
        </>
    )



}