import React, { useState } from "react";
import { Carousel, Image } from "antd";
import { GetCarusel } from "../Services/REST";

export function Carrusel() {

    const [img] = useState(GetCarusel());
    return (
        <Carousel autoplay dots={true} dotPosition={"bottom"} effect={"fade"}>
            {img.map(item => (
                <>
                    
                    <Image key={item.id} preview={false}
                        height={"90vh"}
                        width={"100%"}
                        src={item.url}
                    ></Image>
                </>
            ))}

        </Carousel>
    );
}