import React, { useState, useEffect } from "react";
import { GetInventario } from "../Services/REST";
import { Tabs, Row, Col } from 'antd';
import { Cart } from "./Cart";
import { motion } from "framer-motion";
import styled from "styled-components";
const { TabPane } = Tabs;


export function Catalogo(data) {
    const [menu, setmenu] = useState("Aretes")
    let catalogo = GetInventario();
    const H1 = styled(motion.h1)`color: #fff;`;
    const H1Variants = {
        initial: { y: 0, opacity: 0 },
        animate: { y: 0, opacity: 1, transition: { delay: 0.5 } }
    };
    useEffect(() => {
        setmenu(data.item)
    }, [data.item]);

    let cambio = evento => {
        setmenu(evento);
    }
    

    
    return (
        <>
        
            <H1 variants={H1Variants} initial="initial" animate="animate">
                <Tabs defaultActiveKey={"Aretes"} onChange={cambio} activeKey={menu}>
                    {catalogo.map((item) => {
                        return (
                            <TabPane tab={item.nombre} key={item.nombre} >

                                <Row gutter={[16, 24]}>
                                    {
                                        console.log("lista :", item)
                                    }
                                    {
                                        item.items.map((it, index) => {
                                            return (
                                                <Col key={index} xs={{ span: 12 }} md={{ span: 8 }} lg={{ span: 6 }} >
                                                    <Cart data={it} click={data.seleccionar}/>
                                                </Col>
                                            );
                                        })
                                    }
                                </Row>
                            </TabPane>
                        )
                    })}
                </Tabs>
            </H1>
        </>
    );
}