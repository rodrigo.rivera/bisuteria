
import React, { useEffect, useState } from 'react';
import { Row, Col, Image, Descriptions } from 'antd';
import { GetInventario } from "../Services/REST";
import "../Styles/Descripcion.css";
export function Descripcion(props) {

    
    const [articulo, setArticulo] = useState(null);

    const buscar = temp => {
        let data = GetInventario();
        let encontro = null;
        data.forEach(categoria => {
            categoria.items.forEach(element => {

                if (element.id === parseInt(temp, 10)) {
                    encontro = element;
                    return element;
                }
            })

        });

        //console.log("[se encontro ]", encontro)
        return encontro
    }

    useEffect(() => {
        //console.log("[Descripcion]", props.producto)
        //console.log("Se encotro ", buscar(props.producto));
        let encontro = buscar(props.producto);
        setArticulo(encontro === null ? null : encontro);


    }, [props.producto]);

    const InsertarImagenes = imagenes => {
        
        const items = []
        imagenes.url.forEach(element => {
            items.push(
                <Image
                    height={270}
                    width={320}
                    src={element}
                />
            )
        });

        return (
            <Image.PreviewGroup >
                {items}
            </Image.PreviewGroup>
        )
    }

    //console.log("[Descripcion] ", props)
    return (
        <Row gutter={[16, 24]} className="descripcion">

            <Col xs={{ span: 24 }} lg={{ span: 16 }} >
                
                {articulo === null || typeof (articulo.url) === "string" ? "no hya imagen" :
                    InsertarImagenes(articulo)
                }
            </Col>

            {articulo !== null ? <Col xs={{ span: 24 }} lg={{ span: 7 }} >
                <div className="centrar">
                    <Descriptions span={12} title={articulo.titulo}>
                        <Descriptions.Item span={12} label="ID">
                            {articulo.id}
                        </Descriptions.Item>
                        <Descriptions.Item span={12} label="Nombre">
                            {articulo.titulo}
                        </Descriptions.Item>
                        <Descriptions.Item span={12} label="Descripcion">
                            {articulo.descripcion}
                        </Descriptions.Item>
                        <Descriptions.Item span={12} label="Precio 1 a 3 piezas">
                            ${articulo.precio}.00
                        </Descriptions.Item>
                        <Descriptions.Item span={12} label="Precio 4 a 6 piezas">
                            ${articulo.precio01}.00
                        </Descriptions.Item>
                        <Descriptions.Item span={12} label="Precio 7 a 10 piezas">
                            ${articulo.precio02}.00
                        </Descriptions.Item>

                        <Descriptions.Item span={12} label="Disponible">
                            {articulo.disponible ? "Si" : "No "}
                        </Descriptions.Item>
                    </Descriptions>
                </div>
            </Col>
                : null}


        </Row>
    )

}